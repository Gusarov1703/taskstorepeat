package test9;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите число для проверки на палиндромность: ");
        int number = scanner.nextInt();
        System.out.printf("Число%sявляется палиндромом!%n", isPalindrome(Math.abs(number)) ? " " : " не ");
    }
    public static boolean isPalindrome(int number) {
        StringBuffer buffer = new StringBuffer();
        String number_str = String.valueOf(number);

        buffer.append(number_str);

        buffer = buffer.reverse();
        return buffer.toString().equals(number_str);
    }
}